import java.util.HashSet;
import java.util.Set;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Player {
    public static void main(String[] args) {
        //On instancie des artistes
        Artist miley = new Artist("Miley", "Cyrus");
        Artist hoshi = new Artist("Hoshi", "");
        Artist aya = new Artist("Aya", "Nakamura");
        Artist kendji = new Artist("Kendji", "Girac");
        Artist vianney = new Artist("Vianney", "");

        //Instanciation de l'ensemble des artistes qui jouent la musique
        Set<Artist> duoBisous = new HashSet<>();
        Set<Artist> artistMiley = new HashSet<>();
        Set<Artist> artistHoshi = new HashSet<>();
        Set<Artist> duoFeu = new HashSet<>();

        //Ajout d'artistes
        artistMiley.add(miley);
        artistHoshi.add(hoshi);
        duoBisous.add(aya);
        duoBisous.add(kendji);
        duoFeu.add(vianney);
        duoFeu.add(kendji);

        //On instancie des musiques
        Music flowers = new Music("Flowers", 165, artistMiley);
        Music feu = new Music("Le feu", 182, duoFeu);
        Music bisous = new Music("Bisous", 190, duoBisous);
        Music partirai = new Music("Je partirai", 165, artistHoshi);

        //On instancie une nouvelle playlist
        Playlist favorite = new Playlist();

        //Ajout de musique à mes favoris
        favorite.add(flowers);
        favorite.add(bisous);
        favorite.add(feu);

        //On retourne la playlist
        System.out.println("Mes favoris");
        favorite.showPlaylist();

        //On supprime une musique de la playlist favorite
        favorite.remove(bisous);

        //On retourne de nouveau la playlist favorite pour vérifier la suppression
        System.out.println("Mes nouveaux favoris");
        favorite.showPlaylist();

        //On retourne la durée totale de la playlist
        System.out.println("Durée de la playlist : " + favorite.getTotalDuration());

        //On retourne la musique suivante
        favorite.next();
        favorite.next();
    }
}