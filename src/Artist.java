public class Artist {
    private String firstName;
    private String lastName;

    public Artist(
            String firstName,
            String lastName
    ){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    //C'est mon getter
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }
}
