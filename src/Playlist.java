import java.util.ArrayList;

public class Playlist {
    private Music currentMusic;
    private ArrayList<Music> musicList = new ArrayList<>();
    public void add(Music music){
        musicList.add(music);
    }
    public void remove(Music music){
        musicList.remove(music);
    }

    public void showPlaylist(){
        //On boucle sur les musique de la playlist
        for (var music : musicList){
            System.out.println(music.getInfos());
            System.out.println("\n");
        }
    }

    public String getTotalDuration(){
        int totalDuration = 0;
        for(var music : musicList){
            totalDuration += music.getDuration();
        }
        return Helper.formatDuration(totalDuration);
    }

    public void next(){
        if (!this.musicList.isEmpty()) {
            var first = this.musicList.getFirst();
            this.currentMusic = first;
            this.remove(first);
            System.out.println("La musique jouée est :" + this.currentMusic.getTitle());
        } else {
            System.out.println("Playlist vide");
        }
    }
}
