import java.util.HashSet;
import java.util.Set;

public class Music {
    private String title;
    private int duration;
    private Set<Artist> artistSet;
    public Music(
       String title,
       int duration,
       Set<Artist> artistSet
    ){
         this.title = title;
         this.duration = duration;
         this.artistSet = artistSet;
    }
    public String getTitle(){
        return this.title;
    }

    public int getDuration(){
        return this.duration;
    }

    public String formatDuration(){
        return Helper.formatDuration(this.duration);
    }
    public String getInfos(){
        StringBuilder artistList = new StringBuilder();
        for (var artist : artistSet) {
            if(artistSet.size() > 1) {
                artistList.append(artist.getFullName()).append(" ");
            } else {
                artistList.append(artist.getFullName());
            }
        }

        return "Titre - \"" + this.title + "\" \n"
                + "Durée - " + formatDuration() + "\n"
                + "Artiste - " + artistList;
    }
}
