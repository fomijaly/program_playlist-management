public class Helper {
    public static String formatDuration(int duration){
        int minutes = duration / 60;
        int sec = duration % 60;

        return minutes + ":" + sec;
    }
}
